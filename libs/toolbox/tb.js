

var tb = {
		debug : function(msg) { console.log('TB: ' +msg); },
		ext : {
			DataView : {
				getString : function (view, start, len) {
					var ret = String();
	
					for (var i = start; i < len; i++) {
						var int = view.getUint8(i);
						var char = String.fromCharCode(int);
						ret += char;
					}
					return ret;
				}
			},
			Uint8Array : {
				readInt : function readInt(array, start, bytesCount) {
					var ret = 0,
					shft = 0;
					
					var i = start;
					var bytes = bytesCount;
					
					while (bytes) {
						ret += array[i] << shft;
						shft += 8;
						i++;
						bytes--;
					}
					return ret;
				}
			},
		},
		array:{
			convert : {
				Uint8ArrayToFloat32Array: function(u8a) {
					var f32Buffer = new Float32Array(u8a.length);
					for (var i = 0; i < u8a.length; i++) {
						var value = u8a[i<<1] + (u8a[(i<<1)+1]<<8);
						if (value >= 0x8000) value |= ~0x7FFF;
						f32Buffer[i] = value / 0x8000;
					}
					return f32Buffer;
				}
			} //-- convert
		},
		wave: {
			
			asObject : function(wave) 
			{
				var wave = new Uint8Array(wave);
				
//				console.log("get Settings" +wave);
				function readInt(i, bytes) {
					var ret = 0,
						shft = 0;

					while (bytes) {
						ret += wave[i] << shft;
						shft += 8;
						i++;
						bytes--;
					}
					return ret;
				}
				if (readInt(20, 2) != 1) throw 'Invalid compression code, not PCM ('+readInt(20,2)+')';
				if (readInt(22, 2) != 1) throw 'Invalid number of channels, not 1';
				return {
					sampleRate: readInt(24, 4),
					bitsPerSample: readInt(34, 2),
					samples: wave.subarray(44)
				}	
			},	

		//------------------------------------------------------
			// input:
			// 		wave - ArrayBuffer representing wave
			get :{
				
			} //-- get
		} //-- wave
}