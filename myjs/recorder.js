//-------------------------------------------------------------------
var Recorder = function()
{
	//---------------------------------------
	var EnumState = {
			unknown: 0,
			hidden: 1,
			recording: 2,
			stopped: 3
	}
	
	//---------------------------------------
	// Record Button
	//---------------------------------------
	var RecordButton = function(recorder)
	{
		this.recorder 	= recorder;
		this.labels 	= {stopped:'record', recording:'stop'};
		this.state 		= EnumState.hidden;
		this.div		= $('#recorder-div');
		this.button 	= $("<button>", {
								id: "recorder-btn", 
								class: "btn btn-lg user-btn",
								type: 'button',
								style: 'display:none'});

		var self = this;
		this.button.click(function() {
			if (self.state == EnumState.recording ) self.setState( EnumState.stopped );
			else if (self.state == EnumState.stopped ) self.setState( EnumState.recording );
		});
		
		this.div.append(this.button);		
		this.debug = function(msg) { console.log("RECORD BUTTON: "+msg);}
	}
	
	//---------------------------------------
	RecordButton.prototype.setState = function(state)
	{
		//-------------
		var btnSwitch = function(btn, fn1, fn2)
		{
			var dur = 300;
			btn.fadeOut(dur, function(){
				fn1();
				btn.fadeIn(dur, function(){
					fn2();					
				});
			});
			
		}

		//-------------
		var btnRecord = function(fn){
			btnSwitch( 
					$('#recorder-btn'),
					function() {
						$('#recorder-btn').html( self.labels.stopped);
						$('#recorder-btn').removeClass('btn-primary');
						$('#recorder-btn').addClass('btn-danger');
					},
					function(){
						self.recorder.audioProcessor.setEnable( false );
						self.recorder.videoProcessor.setEnable( false );
						fn();
					})
		}
		
		//-------------
		var btnStop = function(){
			btnSwitch( 
					$('#recorder-btn'),
					function() {
						$('#recorder-btn').html( self.labels.recording);
						$('#recorder-btn').addClass('btn-primary');
						$('#recorder-btn').removeClass('btn-danger');
					},
					function(){
						self.recorder.audioProcessor.setEnable( true );
						self.recorder.videoProcessor.setEnable( true );
					});
		}
		
		this.debug("currnet state: " + this.state + " change to: " + state);

		var self = this;
		switch (state)
		{
			case EnumState.stopped:
				if ( this.state == EnumState.hidden )
				{
					btnRecord( function(){} );
				}
				else	
				if ( this.state == EnumState.hidden || this.state == EnumState.recording )
				{
					btnRecord( function() {
						// getting mp3 data
						self.recorder.audioProcessor.getMp3Data(function( data )
						{
							self.recorder.recordData.audio = data;
							self.recorder.checkDataComplete();
						});
						
						self.recorder.videoProcessor.getGifBlob(function( blob ){
//							console.log(blob);
							self.recorder.recordData.video = blob;
							self.recorder.checkDataComplete();
						});

					});
				}
				this.state = EnumState.stopped;
			break;
				
			case EnumState.recording:
				if ( this.state == EnumState.hidden || this.state == EnumState.stopped )
				{
					btnStop();
				}
				this.state = EnumState.recording;
			break;

			default:
				;
		}
	}
	
	//---------------------------------------
	// Recorder Button
	//---------------------------------------
	this.port = Config.server.media.binary.port;
	this.upstream = {};
	this.downstream = null;
	this.downstreamCount = 0;
	this.audioProcessor = new AudioProcessor();
	this.videoProcessor = new VideoProcessor();
	
	this.streamVideo = false;
	this.streamAudio = false;
	
//	this.blobs = {};
	this.recordData = {};
	this.recordBtn = new RecordButton( this );
	this.recordPreview = new RecordPreview();
	this.recordBtn.setState(EnumState.stopped);
}
//-------------------------------------------------------------------
Recorder.prototype.setup = function() {
	
	var self = this;
    
    //---------------------------------------
    this.mediaSucess = function(stream) {
    	self.videoProcessor.setup( stream );
        self.audioProcessor.setup( stream );
    };

    //---------------------------------------
    this.mediaFail = function() {
    	self.debug('no webcam access :-(');
    };

    //---------------------------------------
    var userMedia 	= Modernizr.prefixed('getUserMedia', navigator);
    userMedia({video: true, audio: true}, this.mediaSucess, this.mediaFail );

}
//-------------------------------------------------------------------
Recorder.prototype.debug = function(msg) { console.log("MEDIA SENDER: "+msg);}
//-------------------------------------------------------------------
Recorder.prototype.postWave = function() { this.audioProcessor.postWave(); }
//-------------------------------------------------------------------
Recorder.prototype.postGif = function() { this.videoProcessor.postGif(); }
//-------------------------------------------------------------------
Recorder.prototype.checkDataComplete = function() { 
	this.debug("checking if data complete"); 
	
	if ( typeof this.recordData.audio === 'undefined' ) return;
	if ( typeof this.recordData.video === 'undefined' ) return;
	
	var self = this;
	this.debug("both blobs are here");
	
	this.recordPreview.show( this.recordData );
}
//-------------------------------------------------------------------
Recorder.prototype.startVideoRecord = function() {
	
}
//-------------------------------------------------------------------
Recorder.prototype.stopVideoRecord = function() {
	
}
//-------------------------------------------------------------------
Recorder.prototype.startAudioRecord = function() {
	
}
//-------------------------------------------------------------------
Recorder.prototype.stopAudioRecord = function() {
	
}
//-------------------------------------------------------------------

Recorder.prototype.toggleVideo = function() { 
	this.streamVideo = ! this.streamVideo;
//	this.videoProcessor.setEnable( this.streamVideo );
	this.updateControlVideo(); 
}
//-------------------------------------------------------------------
Recorder.prototype.toggleAudio = function() { 
	this.streamAudio = ! this.streamAudio; 
	this.audioProcessor.setEnable( this.streamAudio );
	this.updateControlAudio(); 
}
//-------------------------------------------------------------------
//Recorder.prototype.binaryClientSetup = function()
//{
//	var self = this;
//	if ( this.port == -1 )
//	{
//		alert("Port unset");
//		return;
//	}
//	
//	var url = 'ws://' + Config.server.media.host + ':' + this.port;
//	var client = new BinaryClient(url);
//	this.debug("connecting "+url);
//
//	//------------------------------
//	client.on('open', function (s) {
//		self.debug("binarry connected");
//		
//		var meta = { dir: 'toserver', type: 'video'};
//		self.upstream.video = client.createStream(meta);
//		self.startSendingVideo();
//		
//		var meta = { dir: 'toserver', type: 'audio'};
//		self.upstream.audio = client.createStream(meta);
//	});
//
//	//------------------------------
//	client.on('close', function(stream)
//	{
//		self.debug("client close");
//	});
//	
//	//------------------------------
//	client.on('stream', function (stream, meta) {
//		self.debug("binarry stream recieving, meta: "+meta.dir +  "-" +meta.type);
//
//	    if (meta.dir === 'fromserver' && meta.type ==='video')
//	    	handleVideoStream(stream);
//	    else if (meta.dir === 'fromserver' && meta.type ==='audio')
//	    	handleAudioStream(stream);
//	    else
//	    	self.debug("unknow connection, meta:"+JSON.stringify(meta));
//	});
//
//	//------------------------------
//	var handleAudioStream = function(stream)
//	{
//		stream.on('data', function(data) {
//			self.debug("recieving audio data");
//
////			var dataArr = new Float32Array(data);
////			self.audioProcessor.playBuffer(dataArr);
//			self.audioProcessor.test(data);
//    	});
//	}
//	
//	//------------------------------
//	var handleVideoStream = function(stream)
//	{
//		return;
//    	var idCount = self.downstreamCount;
//    	var idName = "downstream-canvas-"+stream.id;
//	    	
//    	self.downstreamCount++;
//	    	
//    	$( "#others-div" )
//    		.append($('<canvas></canvas>')
//	        .attr({ id : idName })
//	        .addClass("downstream-canvas"));
//	    	
//    	self.debug("creating canvas " +idName);
//    	self.debug("from server stream"); 
//
//        var receiverEl = document.getElementById(idName);
//        var receiverContext = receiverEl.getContext('2d');
//
//
//        receiverEl.width = Config.app.canvasWidth;
//        receiverEl.height = Config.app.canvasHeight;
//
//        self.videoProcessor.processStream( stream, receiverContext );
//	}
//}
//-------------------------------------------------------------------
//Recorder.prototype.lastFrameCompare = function( data ) {
//	if (this.lastFrame ==null )
//		return;
//	
//	var delta = 20;
//	var diff = 0;
//	var dr=db=0;
//	var same = 0;
//	var total = 0;
//	for ( i=0; i<data.length; i += 4 )
//	{
//		total++;
//		if (Math.abs(data[i]-this.lastFrame[i]) > delta || 
//			Math.abs(data[i+1]-this.lastFrame[i+1]) > delta || 
//			Math.abs(data[i+2]-this.lastFrame[i+2]) > delta )
//		{
//			diff++;
//			if ( i>0 && (Math.abs(data[i-1]-this.lastFrame[i-1]) > delta || 
//				Math.abs(data[i+0]-this.lastFrame[i+0]) > delta || 
//				Math.abs(data[i+1]-this.lastFrame[i+1]) > delta ))
//			{
//				dr++
//			}
//			else db++;
//		}
//		else
//		{
//			same++;
//		}
//	}
////	this.debug("diff count: "+Math.round(diff/total*100)+" %");
////	this.debug("row:"+dr+"  break:"+db);
////	this.debug("diff count: "+diff+"("+Math.round(diff/total*100)+"%) same:"+same+"("+Math.round(same/total*100)+"%) total:"+total);
//}
//-------------------------------------------------------------------




var btnToggle = function(e, text, addClass, removeClass)
{
	e.fadeOut('slow',function(){
		e.addClass(addClass);
		e.removeClass(removeClass);
		e.html(text);
		e.fadeIn('slow',function(){
			
		});
	});
}