Config = {
	app : {
        grabRate	: 120,
        canvasWidth	: 280,
        canvasHeight: 210,
	},

	sound : {
		sampleRate : 11025,
		bufferSize: 16384
	},
	
	server : {
//		control : {
//			host : '192.168.3.10',
//			port : 9999	
//		},
		
		media : {
			host : '192.168.3.2',
			binary : {
				port :7778
			},
			control : {
				port :7788
			}
		}
	}
};