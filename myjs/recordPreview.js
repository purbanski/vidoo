//---------------------------------------------------------
var RecordPreview = function()
{
	var self = this;
	this.generatedDiv = document.getElementById('generated-div');
	
	//-----------------------
	this.addAudioBlob = function(blob)
	{
		var url = URL.createObjectURL(blob);
		var li = document.createElement('li');
		var au = document.createElement('audio');
		var hf = document.createElement('a');
		      
		au.controls = true;
		au.loop = true;
		au.src = url;
		hf.href = url;
		hf.download = new Date().toISOString() + '.mp3';
		hf.innerHTML = hf.download;
		li.appendChild(au);
		li.appendChild(hf);
		self.generatedDiv.appendChild(li);
//		au.play();
	}

	//-----------------------
	this.addImgBlob = function(blob)
	{
		var url = URL.createObjectURL(blob);
		var li = document.createElement('li');
		var au = document.createElement('img');
		var hf = document.createElement('a');
		      
		au.src = url;
		hf.href = url;
		hf.download = new Date().toISOString() + '.mp3';
		hf.innerHTML = hf.download;
		li.appendChild(au);
		li.appendChild(hf);
		self.generatedDiv.appendChild(li);
	}
}
//---------------------------------------------------------
RecordPreview.prototype.show = function( avData )
{
//	console.log(avData.audio.subarray(0, avData.audio.lenght));
	var mp3data = avData.audio.subarray(0, avData.audio.lenght);
	var mp3Blob = new Blob([mp3data], {type: 'audio/mp3'});
	this.addAudioBlob( mp3Blob );

	this.addImgBlob( avData.video );
}
//---------------------------------------------------------





//---------------------------------------------------------
