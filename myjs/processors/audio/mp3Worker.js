importScripts('/libs/lame/libmp3lame.min.js');
//------------------------------------------------------------------------
var MP3buf = function()
{
	this.buffers = [];
	this.totalLen = 0;
	
	this.debug = function(msg){console.log("MP3buf: "+msg);}
	this.debugMark = function(msg){console.log("-------------------");}
}
//------------------------------------------------------------------------
MP3buf.prototype.push = function( buf )
{
	this.totalLen +=  buf.length;
	this.buffers.push(buf);
//	this.debug("mp3 buf count: "+this.buffers.length+" total len: " + this.totalLen );
}
//------------------------------------------------------------------------
MP3buf.prototype.getData = function()
{
	var data = new Uint8Array(this.totalLen /2 );
	var offset = 0;
	for (i=0; i<this.buffers.length; i++)
	{
		var e = this.buffers[i];
		
		data.set(e.subarray(0, e.length/2),offset);
		offset += (e.length / 2 );
	}
	
	return data;
}
//------------------------------------------------------------------------
MP3buf.prototype.clear = function()
{
	this.buffers = [];
	this.totalLen = 0;
}
//------------------------------------------------------------------------
var debug = function(msg) {
	console.log("MP3 WORKER: " + msg );
}
//------------------------------------------------------------------------
var mp3codec;
var mp3buf = new MP3buf();

this.onmessage = function(e) {
//	debug("cmd "+e.data.cmd);
	switch (e.data.cmd) {
		case 'init':
			debug('init');
			
			if (!e.data.config) {
				e.data.config = { };
			}
			mp3codec = Lame.init();
	
			Lame.set_mode(mp3codec, e.data.config.mode || Lame.JOINT_STEREO);
			Lame.set_num_channels(mp3codec, e.data.config.channels || 2);
			Lame.set_num_samples(mp3codec, e.data.config.samples || -1);
			Lame.set_in_samplerate(mp3codec, e.data.config.samplerate || 44100);
			Lame.set_out_samplerate(mp3codec, e.data.config.samplerate || 44100);
			Lame.set_bitrate(mp3codec, e.data.config.bitrate || 128);
	
			Lame.init_params(mp3codec);
			console.log('Version :', Lame.get_version() + ' / ',
				'Mode: '+Lame.get_mode(mp3codec) + ' / ',
				'Samples: '+Lame.get_num_samples(mp3codec) + ' / ',
				'Channels: '+Lame.get_num_channels(mp3codec) + ' / ',
				'Input Samplate: '+ Lame.get_in_samplerate(mp3codec) + ' / ',
				'Output Samplate: '+ Lame.get_in_samplerate(mp3codec) + ' / ',
				'Bitlate :' +Lame.get_bitrate(mp3codec) + ' / ',
				'VBR :' + Lame.get_VBR(mp3codec));
			break;
			
		case 'encode':
			debug('encode');
			var mp3 = Lame.encode_buffer_ieee_float(mp3codec, e.data.buf, e.data.buf, e.data.buf.length);
			mp3buf.push(mp3.data);
			mp3 = Lame.encode_flush(mp3codec);
			mp3buf.push(mp3.data);

//			self.postMessage({cmd: 'encode', mp3: mp3data});
			break;
			
		case 'getData' :
			debug('getData');
			var mp3 = Lame.encode_flush(mp3codec);
			mp3buf.push(mp3.data);

			var uintArr = mp3buf.getData();
			self.postMessage({cmd:'getData',mp3: uintArr});
			break;
			
		case 'clear' :
			mp3buf.clear();
			break;
			
		case 'finish':
//			var mp3data = Lame.encode_flush(mp3codec);
//			self.postMessage({cmd: 'end', buf: mp3data.data});
//			Lame.close(mp3codec);
//			mp3codec = null;
			break;
	}
};
//------------------------------------------------------------------------
