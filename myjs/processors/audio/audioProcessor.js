var WORKER_PATH		 ='/myjs/processors/audio/';
var WAVE_WORKER_PATH = WORKER_PATH + 'recordWorker.js';
var MP3_WORKER_PATH  = WORKER_PATH + 'mp3Worker.js';
//-------------------------------------------------------------------
var AudioProcessor = function()
{
    window.AudioContext = window.AudioContext || window.webkitAudioContext;

    this.audioNode = null;
    this.audioContext = new AudioContext();
    this.audioContext.sampleRate = Config.sound.sampleRate;
    
    this.inputPoint = this.audioContext.createGain();
    
    this.workers = {};
    this.workers.wave = new Worker( WAVE_WORKER_PATH);
    this.workers.mp3 = new Worker( MP3_WORKER_PATH );
    
    this.callbacks = {};
    this.enable = false;
}
//-------------------------------------------------------------------
AudioProcessor.prototype.toggleEnable 	= function(){ this.enable = !this.enable; }
AudioProcessor.prototype.getEnable 		= function(){ return this.enable; }
AudioProcessor.prototype.setEnable 		= function(e) { this.enable = e; }
//-------------------------------------------------------------------
AudioProcessor.prototype.getMp3Data = function( fn )
{
	this.callbacks.getMp3Data = fn;
	this.workers.mp3.postMessage({ cmd: 'getData' });
}
//-------------------------------------------------------------------
AudioProcessor.prototype.debug = function(msg){console.log("AUDIO PROCESSOR: "+msg);}
//-------------------------------------------------------------------
AudioProcessor.prototype.setup = function(stream )
{
	this.debug("setup");
	var self = this;

	//---------------------------------
    // setup audio nodes
    //---------------------------------
    this.audioInput = this.audioContext.createMediaStreamSource(stream);
    this.audioInput.connect(this.inputPoint);

//    var analyserNode = audioContext.createAnalyser();
//    analyserNode.fftSize = 2048;
//    inputPoint.connect( analyserNode );

    var bufferLen = Config.sound.bufferSize;
    var audioInputContext = this.inputPoint.context;
    
    if( ! audioInputContext.createScriptProcessor)
    	this.audioNode = audioInputContext.createJavaScriptNode(bufferLen, 1, 1);
    else
    	this.audioNode = audioInputContext.createScriptProcessor(bufferLen, 1, 1);
    
    
    this.inputPoint.connect(this.audioNode);
    this.audioNode.connect(audioInputContext.destination); 
  
//    zeroGain = this.audioContext.createGain();
//    zeroGain.gain.value = 0;
//    this.inputPoint.connect( zeroGain );
//    zeroGain.connect( this.audioContext.destination );

    //---------------------------------
    // On audio process
    //---------------------------------
    this.audioNode.onaudioprocess = function(e) 
    {
    	if (!self.enable )
    		return;

    	var data = e.inputBuffer.getChannelData(0);
		var buf = new Float32Array(data.length);
		buf.set(data,0);

		self.workers.wave.postMessage({
			command: 'getWave',
			buffer: e.inputBuffer.getChannelData(0) });
    }
      
    //---------------------------------
    // WAVE init
    //---------------------------------
    this.workers.wave.postMessage({ 
    	command: 'init',
    	config: { sampleRate: self.audioContext.sampleRate}
      });
    
    //---------------------------------
    // WAVE worker callback
    //---------------------------------
	this.workers.wave.onmessage = function(e) {
//		self.debug("message recieved: "+e.data.cmd);

		switch(e.data.cmd) {
    		case 'record':
    			self.dataListenerCb( e.data.buf );
    			break
    		
    		case 'getWave':
    			var wave = tb.wave.asObject( e.data.wave );
    			self.workers.mp3.postMessage({ 
    				cmd: 'encode', 
    				buf: tb.array.convert.Uint8ArrayToFloat32Array(wave.samples) });
    			break;

    		default:
    			self.debug("Command unknown");
      }
    }    

    //---------------------------------
    // MP3 init
    //---------------------------------
	this.workers.mp3.postMessage({ 
		cmd: 'init', 
		config:{
			mode : 3,
			channels:1,
			samplerate: 44100,
			bitrate: 256 }});
	
    //---------------------------------
    // MP3 worker callback
    //---------------------------------
	this.workers.mp3.onmessage = function(e) {
		self.debug("message recieved: "+e.data.cmd);
		switch(e.data.cmd) {

			case 'getData' :
				self.debug("mp3 len: "+e.data.mp3.length);
				self.callbacks.getMp3Data( e.data.mp3 );
			break;

    		default:
    			self.debug("Command unknown");
      }
    }
}
////-------------------------------------------------------------------
//AudioProcessor.prototype.setupDownload = function(blob, filename) {
//	this.debug("setting download data:"+ blob +" file:"+filename);
//    var url = (window.URL || window.webkitURL).createObjectURL(blob);
//    
//    var link = document.getElementById("save");
//    link.href = url;
//    link.download = filename || 'output.wav';
// }
//-------------------------------------------------------------------
AudioProcessor.prototype.playBuffer = function( buffer ) {

//	this.debug("playing buffer (chunks), len: " + buffer.length );
//	this.debug("buff size: " + this.audioNode.bufferSize);
//	this.debug("sample rate: " + this.audioContext.sampleRate);
   
	
	var startTime = 0;

	var audioChunk = buffer;
	var audioBuffer = this.audioContext.createBuffer(
			1, 
			buffer.length, 
			this.audioContext.sampleRate );// );
		
	audioBuffer.getChannelData(0).set(audioChunk);
		
	var source = this.audioContext.createBufferSource();
	source.buffer = audioBuffer;
	source.loop = false;
	source.start(startTime);
	source.connect(this.audioContext.destination);

	startTime += audioBuffer.duration;
 }
//-------------------------------------------------------------------
