//-------------------------------------------------------------------
var VideoProcessor = function() {

	this.config = { workers: 8, quality: 6 };
	
	this.enable = false;
	this.user 	= {};
	this.user.video 	= document.getElementById('user-video');
	this.user.canvas 	= document.getElementById('user-canvas');
	this.user.ctx		= this.user.canvas.getContext('2d');
}
//-------------------------------------------------------------------
VideoProcessor.prototype.setEnable = function(enable) { 
	this.enable = enable;
	if (enable)
		this.gif = new GIF({
			workers: this.config.workers,
			quality: this.config.quality,
			width: Config.app.canvasWidth,
			height: Config.app.canvasHeight
		});
}
//-------------------------------------------------------------------
VideoProcessor.prototype.debug = function(msg) {console.log("VIDEO PROCESSOR: "+msg);}
//-------------------------------------------------------------------
VideoProcessor.prototype.setup = function( stream )
{
    this.user.canvas.width = Config.app.canvasWidth;
    this.user.canvas.height = Config.app.canvasHeight;
    
    this.debug('cam and mic access granted :-)');
    this.user.video.src = window.URL.createObjectURL(stream);
    this.user.video.play();
    this.debug('video play');
    
    this.grabLoop();
}
//---------------------------------------
VideoProcessor.prototype.grabLoop = function () 
{
	// fix desync on start 
	// cause we started.. we are checking each 
	var self = this;
	var grab = function()
	{
	    if ( ! self.enable ) {
			setTimeout(grab, 100);
	    	return;
	    }

		self.debug('grabbing video');
	    try {
	        self.user.ctx.drawImage(
	        		self.user.video, 0, 0, 
	        		Config.app.canvasWidth, Config.app.canvasHeight );
	    } catch (e) {}

//	    var imageData = self.user.ctx.getImageData(
//	    		0, 0, Config.app.canvasWidth, Config.app.canvasHeight );

//	    console.log(self.user.canvas.toDataURL("image/png"););
//	    var imgData = self.user.canvas.toDataURL('image/png');
//	    var img = document.createElement('img');
//	    img.src = imgData;
//	    document.body.appendChild(img);
	    
	    self.processFrame();
		setTimeout(grab, Config.app.grabRate);
	}
	grab();
}
//-------------------------------------------------------------------
VideoProcessor.prototype.getGifBlob = function( cbFn ) {
	var self = this;
	
	this.debug("post gif");
	
	this.gif.on('finished', function(blob){
		self.debug('rendering gif finished');
		cbFn(blob);

	});
	this.gif.render();
}
//-------------------------------------------------------------------
VideoProcessor.prototype.processFrame = function() {
	if (typeof this.frameTime === 'undefined' )
		this.frameTime = _.now();
	
//	console.log( _.now() - this.frameTime );
	
	var delay = _.now() - this.frameTime ;
	this.frameTime = _.now();

	this.gif.addFrame(this.user.ctx, {copy:true, delay:delay});
//	this.gif.render();
}
//-------------------------------------------------------------------

//-------------------------------------------------------------------

